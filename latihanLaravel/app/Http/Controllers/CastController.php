<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('cast')->get();
        return view('cast.index',['casts'=>$casts]);
    }
    public function create()
    {
        return view('cast.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        
        DB::table('cast')->insert([
            'nama' => $request['name'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    
    }
    public function show($id){
        $cast = DB::table('cast')->find($id);
        return view('cast.show', ['cast'=>$cast]);
    }
    public function edit($id){
        $cast = DB::table('cast')->find($id);
        return view('cast.edit', ['cast'=>$cast]);
    }
    public function update($id, Request $request){
        $request->validate([
            'name' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        DB::table('cast')
        ->where('id', $id)
        ->update(
            [
                'nama' => $request['name'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );
     
        return redirect('/cast');
    }
    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
