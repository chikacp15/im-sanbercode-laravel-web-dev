<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(){
        return view('pages.form');
    }
    public function register(Request $request){
      $first_name = $request['first_name'];
      $last_name = $request['last_name'];
      $gender = $request['gender'];
      $nationality = $request['nationality'];
      $language = $request['language'];
      $textarea_bio = $request['textarea_bio'];
        
      return view('pages.home', ['first_name'=> $first_name, 'last_name' => $last_name]);
    }
}
