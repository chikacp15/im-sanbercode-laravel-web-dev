<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[DashboardController::class, 'index']);

Route::get('/form', [FormController::class, 'index']);
Route::post('/signup', [FormController::class, 'register']);

Route::get('/data-table',function(){
  return view('pages.data-table');
});
Route::get('/table',function(){
  return view('pages.table');
});

//CRUD CAST

Route::get('/cast/create', [CastController::class,'create']);
Route::post('/cast', [CastController::class,'store']);

Route::get('/cast', [CastController::class,'index']);
Route::get('/cast/{id}', [CastController::class,'show']);

Route::get('/cast/{id}/edit', [CastController::class,'edit']);
Route::put('/cast/{id}', [CastController::class,'update']);
Route::delete('/cast/{id}', [CastController::class,'destroy']);
