@extends('layouts.master')
@section('title', 'Edit Cast')
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Name</label>
      <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$cast->nama}}" >
      @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <label >Umur</label>
      <input type="number" min="0" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{$cast->umur}}">
      @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <label >Bio</label>
      <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" >{{$cast->bio}}</textarea>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
