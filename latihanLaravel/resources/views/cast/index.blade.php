@extends('layouts.master')
@section('title', 'All Cast')
@section('content')
<a href="/cast/create" class="btn btn-secondary">Add Cast</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casts as $keys=>$cast)
        <tr>
            <th scope="row">{{$keys+1}}</th>
            <td>{{$cast->nama}}</td>
            <td>{{$cast->umur}}</td>
            <td>
                <form action="/cast/{{$cast->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$cast->id}}" class="btn btn-primary">Detail</a>
                <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning">Update</a>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
   
        </tr>
        @empty
        <tr>
            <td>Cast Kosong!</td>
        </tr>
        @endforelse
   
    
  </table>
@endsection
