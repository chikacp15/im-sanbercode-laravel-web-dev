@extends('layouts.master')
@section('title', 'Form')
@section('content')
 <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/signup" method="POST">
        @csrf
        <label for="first_name">First name:</label><br><br>
        <input type="text" id="first_name" name="first_name"><br><br>
        <label for="last_name">Last name:</label><br><br>
        <input type="text" id="last_name" name="last_name"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" id="male" name="gender">Male<br>
        <input type="radio" id="female" name="gender">Female<br>
        <input type="radio" id="other" name="gender">Other<br><br>
        <label for="nationality">Nationality</label><br><br>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="other">Other</option>
        </select><br><br>
        <label for="language">Language Spoken:</label><br><br>
        <input type="checkbox" id="bahasa_indonesia" name="language[]">Bahasa Indonesia<br>
        <input type="checkbox" id="female" name="language[]">Bahasa Inggris  <br>
        <input type="checkbox" id="other" name="language[]">Other<br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="textarea_bio" id="textarea_bio" cols="30" rows="10"></textarea><br>
        <button type="submit">Signup</button>
        
    </form>
@endsection
