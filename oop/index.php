<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');
echo "<h1>Latihan OOP</h1>";

$sheep = new Animal("shaun");
echo "Nama: ".$sheep->nama."<br>"; // "shaun"
echo "Legs: ".$sheep->legs."<br>"; // 4
echo "Cold Blooded: ".$sheep->cold_blooded; // "no"
echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Nama: ".$kodok->nama."<br>"; 
echo "Legs: ".$kodok->legs."<br>"; 
echo "Cold Blooded: ".$kodok->cold_blooded."<br>";
echo "Jump: ".$kodok->jump();
echo "<br>";
echo "<br>";

$sungokong = new Ape("Kera sakti");
echo "Nama: ".$sungokong->nama."<br>"; 
echo "Legs: ".$sungokong->legs."<br>"; 
echo "Cold Blooded: ".$sungokong->cold_blooded."<br>";
echo "Yell: ".$sungokong->yell();




